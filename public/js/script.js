$(document).ready(function () {

    var photoDownloaded = false;
    var dropArea = document.querySelector("#dropZone");
    var inputArea = document.querySelector("#photo-input");
    var editBtn = document.querySelector('.load-modal-edit__btn');
    var btnSub = document.querySelector('.load-modal__btn');
    var files;

    $(".showLoginPopup").on("click", function () {
        $("body").addClass("mask");
        $('#loginModal').css('display', 'flex');
    });

    $('#showReg').on('click', function () {
        $("body").addClass("mask");
        $('#registerModal').css('display', 'flex');
        $('#loginModal').css('display', 'none');
    });

    $('input[type=file]').on('change', function(){
        files = this.files;
    });

    $(".stories-filter__link").on("click", function () {
        $(".stories-filter__link").removeClass("active");
        $(this).addClass("active");
    });

    $(".stories-panel__btn").on("click", function () {
        let offsetFromScreenTop = $(window).scrollTop() - $("body").offset().top  + 55;
        $("body").addClass("mask");
        $(".load-modal").css(
           {
               "display": "block",
               "top" : offsetFromScreenTop
           });
    });

    $(".load-modal__close-btn").on("click", function () {
       $("body").removeClass("mask");
        $('#loginModal').css('display', 'none');
        $('#registerModal').css('display', 'none');
        $(".load-modal").css("display", "none");
    });

    function handleFileSelect(evt) {
        var file = evt.target.files; // FileList object
        var f = file[0];
        // Only process image files.
        var reader = new FileReader();
        // Closure to capture the file information.
        reader.onload = (function(theFile) {
            return function(e) {
                // Render thumbnail.
                $(".load-modal__dragndrop-form").css("background-image", "url("+e.target.result+")");
                $(dropArea).css("display", "none");
            };
        })(f);
        // Read in the image file as a data URL.
        reader.readAsDataURL(f);
        $(editBtn).css("display", "block");
        $(".load-modal__filter-container").removeClass("disabled");
        $(".load-modal__btn").removeClass("not-active alt-btn");
        $(".load-modal__btn").addClass("main-btn");

    }

    function deleteFileSelect(evt) {
        $(".load-modal__dragndrop-form").css("background-image", "none");
        $(dropArea).css("display", "flex");

        photoDownloaded = false;
        $(editBtn).css("display", "none");
        $(".load-modal__filter-container").addClass("disabled");
        $(".load-modal__btn").addClass("not-active alt-btn");
        $(".load-modal__btn").removeClass("main-btn");
        dropClick();
    }

    document.querySelector(".load-modal-edit__btn").addEventListener("click", deleteFileSelect, false);
    document.getElementById('photo-input').addEventListener('change', handleFileSelect, false);

    function dropClick() {
        if (!photoDownloaded) {
            inputArea.click();
            photoDownloaded = true;
        }
    }

    $('.load-modal__btn').bind("click", sendFileToLaravelAjax);

    function sendFileToLaravelAjax() {

        console.log('XSsax');

        // Проверяем на заполненость поля
        if( typeof files == 'undefined' ) return;

        // создадим объект данных формы
        var data = new FormData();

        // заполняем объект данных файлами в подходящем для отправки формате
        $.each( files, function( key, value ){
            data.append( key, value );
        });

        $.ajax({
            type: 'POST',
            url: '/upload',
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            data        : data,
            cache       : false,
            // отключаем обработку передаваемых данных, пусть передаются как есть
            processData : false,
            // отключаем установку заголовка типа запроса. Так jQuery скажет серверу что это строковой запрос
            contentType : false,
            success: function (data) {
                $(".load-modal__dragndrop-form").css(
                    {
                        "background-image" : "none",
                        "width" : "100%"
                    });
                $('.load-modal__dragndrop-area').remove();
                document.querySelector('.load-modal__title').innerHTML = "ШАГ 2";
                document.querySelector('.load-modal__description').innerHTML = "Расскажи историю своих кроссовок ASICS";
                document.querySelector('.load-modal__dragndrop-form').innerHTML += "<textarea class=\"load-modal__dragndrop-form-textarea\" placeholder=\"Поделись историей в удобном для тебя формате\" required=\"\" maxlength=\"2000\" name='body'></textarea>";
                document.querySelector('.load-modal__btn-container').innerHTML = "<button class=\"load-modal__btn alt-btn not-active\" type='submit'> Сохранить </button>";
                $(".load-modal__progress-bar-indicator").css('width', '70%');
                document.querySelector('.load-modal__progress-bar-percents').innerHTML = "70%";
                var textarea = document.querySelector('.load-modal__dragndrop-form-textarea');
                textarea.addEventListener("keydown", function () {
                    if (textarea.value.length > 0) {
                        $(".load-modal__btn").removeClass("not-active alt-btn");
                        $(".load-modal__btn").addClass("main-btn");
                    } else {
                        $(".load-modal__btn").addClass("not-active alt-btn");
                        $(".load-modal__btn").removeClass("main-btn");
                    }
                },false);
                $('.load-modal__btn').on("click", function () {
                    $(".load-modal__dragndrop-form").submit();
                });
            },
            error: function (data) {
                alert('Какая-то ошибка((((');
            }
        });
    }

    $('.login-modal__form').on('submit', function(e){
        e.preventDefault();
        let username = $('#login-email').val();
        let password = $('#login-password').val();
        $.ajax({
            url: $(this).attr('action'),
            data: {"email":username,"password": password},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            success: function (html) {
                location.reload();
            },
            error: function (html) {
                $('.invalid-feedback').css('display', 'block');
                $('#login-email').addClass('login-modal__input_error');

            }
        });

        console.log('Submit');
    });
    $('.login-modal__form').on('submit', function(e){
        e.preventDefault();
        let username = $('#reg-name').val();
        let email = $('#reg-email').val();
        let password = $('#reg-password').val();
        let passwordConfirm = $('#reg-confirm-password').val();
        $.ajax({
            url: $(this).attr('action'),
            data: {"name":username, "email":email,"password": password, "password_confirmation": passwordConfirm},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: 'POST',
            success: function (html) {
                location.reload();
            },
            error: function (html) {
                $('.invalid-feedback').css('display', 'block');
                $('#login-email').addClass('login-modal__input_error');

            }
        });

        console.log('Submit');
    });



    dropArea.addEventListener("click", dropClick, false);

});
