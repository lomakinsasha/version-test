<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'PostsController@index');
Route::get('/posts/{post}', 'PostsController@show');
Route::post('/upload', 'ImageController@upload')->name('image.upload');
Route::post('/', 'ImageController@uploadText')->name('image.upload');

Route::get('setlocale/{locale}', function ($locale) {

    if (in_array($locale, \Config::get('app.locales'))) {   # Проверяем, что у пользователя выбран доступный язык
        Session::put('locale', $locale);                    # И устанавливаем его в сессии под именем locale
    }

    return redirect()->back();                              # Редиректим его <s>взад</s> на ту же страницу

});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/login', ['uses'=>'Auth\LoginController@login']);
Route::post('/register', ['uses'=>'Auth\RegisterController@register']);

Route::get('/login/vkontakte', 'AuthController@redirectToProvider');
Route::get('/login/vkontakte/callback', 'AuthController@handleProviderCallback');

//Route::get('/github', 'AuthController@redirectToProvider');
//Route::get('/github/callback', 'AuthController@handleProviderCallback');