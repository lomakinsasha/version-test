<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Routing\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Socialite;

class AuthController extends Controller
{
    /**
     * Переадресация пользователя на страницу аутентификации GitHub.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('vkontakte')->redirect();
    }

    /**
     * Получение информации о пользователе от GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {

        $userSocialite = Socialite::driver('vkontakte')->user();
        $accessTokenResponseBody = $userSocialite->accessTokenResponseBody;
        $findUser = User::Where('email', $accessTokenResponseBody['email'])->first();
        if ($findUser){
            $user = $findUser;
        }else{
            $user = new User;
            $user->name = $userSocialite->getName();
            $user->email = $accessTokenResponseBody['email'];
            $user->author_photo_link = $userSocialite->avatar;
            $user->password = bcrypt(123456);
            $user->save();
        }

        Auth::login($user);

        return redirect('/');
    }
}