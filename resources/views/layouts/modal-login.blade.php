<div class="login-modal modal" style="display: none" id="loginModal">
    <a class="load-modal__close-btn modal-close-btn"></a>

    <div class="login-modal__content">
        <div class="load-modal__header">
            <p class="login-modal__title load-modal__title">
                Войти через
            </p>
        </div>

        <div class="login-modal__social-container">
            <a href="/login/vkontakte" class="login-modal__social-btn">
              <svg class="social-icon" viewBox="0 0 25 25">
            <use xlink:href="#vk-social-icon">
             <svg id="vk-social-icon">
      <g clip-path="url(#clip0)">
        <path d="M24.867 18.25a1.708 1.708 0 0 0-.084-.163c-.434-.781-1.263-1.74-2.486-2.877l-.026-.026-.013-.013-.013-.013h-.013c-.556-.529-.907-.885-1.055-1.067-.269-.347-.33-.699-.182-1.055.104-.269.495-.837 1.171-1.705.356-.46.638-.828.846-1.106 1.502-1.996 2.153-3.271 1.953-3.827l-.078-.13c-.052-.078-.186-.15-.403-.214-.217-.066-.495-.076-.833-.033l-3.749.026a.486.486 0 0 0-.26.007l-.17.039-.065.032-.052.04a.578.578 0 0 0-.143.136.893.893 0 0 0-.13.228 21.26 21.26 0 0 1-1.393 2.928 27.757 27.757 0 0 1-.885 1.4c-.269.395-.494.685-.677.872a4.726 4.726 0 0 1-.494.449c-.148.113-.26.16-.339.143a9.847 9.847 0 0 1-.221-.052.866.866 0 0 1-.293-.319 1.429 1.429 0 0 1-.15-.508 5.57 5.57 0 0 1-.045-.527 11.77 11.77 0 0 1 .006-.625c.01-.269.014-.451.014-.546 0-.33.006-.688.019-1.074l.032-.918c.01-.225.013-.464.013-.716 0-.251-.015-.449-.045-.592a2.026 2.026 0 0 0-.136-.417.702.702 0 0 0-.267-.312 1.503 1.503 0 0 0-.436-.176c-.46-.104-1.046-.16-1.758-.169-1.614-.017-2.65.087-3.11.313a1.753 1.753 0 0 0-.495.39c-.156.19-.178.295-.065.312.52.078.889.265 1.106.56l.078.156c.06.113.122.313.182.599.061.286.1.603.118.95.043.634.043 1.176 0 1.627a20 20 0 0 1-.124 1.054 2.11 2.11 0 0 1-.176.612 2.59 2.59 0 0 1-.156.287.225.225 0 0 1-.065.064.975.975 0 0 1-.352.066c-.121 0-.269-.061-.442-.183a3.126 3.126 0 0 1-.54-.5 6.7 6.7 0 0 1-.632-.892 15.566 15.566 0 0 1-.729-1.354l-.208-.378a32.48 32.48 0 0 1-.533-1.06 20.46 20.46 0 0 1-.6-1.348.858.858 0 0 0-.312-.416l-.065-.04a.89.89 0 0 0-.208-.11 1.381 1.381 0 0 0-.3-.085L.834 6.02c-.364 0-.612.083-.742.248l-.052.078a.421.421 0 0 0-.04.208c0 .096.026.213.078.352a42.963 42.963 0 0 0 1.699 3.54c.611 1.137 1.143 2.053 1.594 2.746.451.695.911 1.35 1.38 1.966.469.616.779 1.01.93 1.184.152.174.272.304.359.39l.325.313c.208.208.514.458.918.749.403.29.85.577 1.34.859.49.282 1.061.512 1.712.69.65.178 1.284.249 1.9.215h1.498c.303-.027.533-.122.69-.287l.051-.065a.865.865 0 0 0 .098-.24c.03-.11.045-.229.045-.359a4.27 4.27 0 0 1 .085-1.008c.065-.3.139-.525.221-.677a1.667 1.667 0 0 1 .502-.573.847.847 0 0 1 .103-.045c.209-.07.454-.002.736.202.282.204.547.455.794.755s.545.635.892 1.008.65.65.91.833l.261.157c.174.104.4.2.677.286.277.087.52.108.729.065l3.332-.052c.33 0 .586-.054.768-.163.182-.108.29-.228.325-.358.035-.13.037-.277.007-.442a1.634 1.634 0 0 0-.091-.345z"></path>
        <g clip-path="url(#clip1)">
          <path d="M18.97 9.712l-.568-.001c-.638 0-1.05.423-1.05 1.078v.497h-.572a.09.09 0 0 0-.09.09v.72c0 .05.04.09.09.09h.571v1.818c0 .049.04.089.09.089h.745c.05 0 .09-.04.09-.09v-1.817h.668a.09.09 0 0 0 .089-.09v-.72a.089.089 0 0 0-.09-.09h-.668v-.421c0-.203.049-.306.313-.306h.382c.05 0 .09-.04.09-.089v-.669a.09.09 0 0 0-.09-.09z"></path>
        </g>
      </g>
    </svg>  
             </use>
          </svg>

            </a>
            <a href="https://runandwin.ru/alleyaslavy/auth/facebook" class="login-modal__social-btn">

            </a>
            <a href="#" class="login-modal__social-btn login-modal__social-btn--asics runandwinLogin">
            </a>
        </div>


        <p class="login-modal__social-decsription">
            или использовать E-mail для входа
        </p>

        <div class="login-modal__allerts allert-message"></div>

        <form action="{{ route('login') }}" class="login-modal__form">
            @csrf
            <span class="invalid-feedback">
                    <strong>Не правильный логин или пароль</strong>
            </span>
            <label class="login-modal__label login-modal__label--email" for="">
                <input id="login-email" type="email" class="login-modal__input login-modal__input--email @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Email" autofocus>

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </label>
            <label class="login-modal__label login-modal__label--pass" for="">
                <input id="login-password" type="password" class="login-modal__input login-modal__input--pass @error('password') is-invalid @enderror" name="password" placeholder="Пароль" required autocomplete="current-password">

                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </label>
            <button type="submit" class="login-modal__submit-btn main-btn">Войти</button>
        </form>

        <p class="login-modal__register-offer">Нет аккаунта? <a id="showReg" onclick="ym(54015763, 'reachGoal', 'register_click');">Зарегистрироваться</a></p>
        <p class="login-modal__register-offer" style="margin-top:0.8vw;"><small><a href="{{ route('password.request') }}" class="resetPass">Забыли пароль?</a></small></p>
    </div>
</div>