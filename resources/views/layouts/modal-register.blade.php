<div class="login-modal register-modal modal" style="display: none" id="registerModal">
    <a class="load-modal__close-btn modal-close-btn"></a>

    <div class="login-modal__content">
        <div class="load-modal__header">
            <p class="login-modal__title load-modal__title">
                Регистрация
            </p>
        </div>


        <form action="{{ route('register') }}" class="login-modal__form">

            <label class="login-modal__label login-modal__label--name" for="">
                <input id="reg-name" type="text" class="login-modal__input @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" placeholder="Имя и Фамилия" required autocomplete="name" autofocus>
            </label>


            <label class="login-modal__label login-modal__label--email" for="">
                <input type="email" id="reg-email" name="email" class="login-modal__input login-modal__input--email @error('email') is-invalid @enderror" value="{{ old('email') }}" placeholder="Email" required="">
            </label>
            <label class="login-modal__label login-modal__label--pass" for="">
                <input type="password" id="reg-password" name="password" class="login-modal__input login-modal__input--pass @error('password') is-invalid @enderror" placeholder="Пароль" required="" aria-autocomplete="list">
            </label>
            <label class="login-modal__label login-modal__label--pass" for="">
                <input type="password" id="reg-confirm-password" class="login-modal__input login-modal__input--pass" name="password_confirmation" placeholder="Подтверждение пароля" required="">
            </label>
            <button type="submit" class="login-modal__submit-btn main-btn" style="display: flex; padding: 23px; justify-content: center; align-items: center;">Зарегистрироваться</button>
        </form>

        <p class="login-modal__register-offer">Уже есть аккаунт? <a id="showLogin">Войти</a></p>
    </div>
</div>