<?php $locale = App::getLocale(); ?>
<div class="m-auto">
    <h1 class="main__title">@lang('nav.title')</h1>
</div>
@if(App::isLocale('en'))
    <form action="setlocale/ru">
        <button class="main-btn" type="submit">ru</button>
    </form>
@else
    <form action="setlocale/en">
        <button class="main-btn" type="submit">en</button>
    </form>
@endif
<div class="stories-panel">
    <div class="stories-filter">
        <ul class="stories-filter__list">
            <li class="stories-filter__list-item"><a class="stories-filter__link active" href="#">@lang('nav.popular')</a></li>
            <li class="stories-filter__list-item"><a class="stories-filter__link" href="#">@lang('nav.DISCUSSED')</a></li>
            <li class="stories-filter__list-item"><a class="stories-filter__link" href="#">@lang('nav.WINNERS')</a></li>
            <li class="stories-filter__list-item"><a class="stories-filter__link" href="#">@lang('nav.HISTORY_2018')</a></li>
        </ul>
    </div>
    <div class="stories-panel__search-container">
        <form action="" class="stories-panel__search">
            <label class="story-search__label" for>
                <input class="story-search__input" type="text" placeholder="@lang('nav.SEARCH')">
            </label>
        </form>
        @if($auth)
            <button class="stories-panel__btn main-btn ">@lang('nav.ADD_HISTORY')</button>
        @else
            <button class="main-btn showLoginPopup">@lang('nav.ADD_HISTORY')</button>
        @endif
    </div>
</div>