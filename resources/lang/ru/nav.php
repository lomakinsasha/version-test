<?php

return [
    'title' => 'АЛЛЕЯ СЛАВЫ БЕГОВЫХ КРОССОВОК ASICS',
    'popular' => 'популярные',
    'DISCUSSED' => 'ОБСУЖДАЕМЫЕ',
    'WINNERS' => 'ПОБЕДИТЕЛИ',
    'HISTORY_2018' => 'ИСТОРИИ 2018',
    'SEARCH' => 'поиск',
    'ADD_HISTORY' => 'добавить историю'
];